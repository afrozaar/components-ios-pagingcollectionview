//
//  CollectionViewCell.m
//  SideScrollCustomLayoutCollectionView
//
//  Created by Grant Spilsbury on 2014/10/17.
//  Copyright (c) 2014 GNS. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    self.layer.borderWidth = 4;
}

@end
