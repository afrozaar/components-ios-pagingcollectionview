//
//  AppDelegate.h
//  SideScrollCustomLayoutCollectionView
//
//  Created by Grant Spilsbury on 2014/10/17.
//  Copyright (c) 2014 GNS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
