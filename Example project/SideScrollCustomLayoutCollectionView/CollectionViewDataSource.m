//
//  CollectionViewDataSource.m
//  SideScrollCustomLayoutCollectionView
//
//  Created by Grant Spilsbury on 2014/10/20.
//  Copyright (c) 2014 GNS. All rights reserved.
//

#import "CollectionViewDataSource.h"

@implementation CollectionViewDataSource

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSInteger arraySize = 10;
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:arraySize];
        for (int i = 0; i < arraySize; ++i) {
            [array addObject:[@(i) stringValue]];
        }
        _items = [NSArray arrayWithArray:array];
    }
    return self;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath {
    return self.items[indexPath.row];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.items count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionViewCell" forIndexPath:indexPath];
    cell.label.text = self.items[indexPath.row];
    return cell;
}

@end
