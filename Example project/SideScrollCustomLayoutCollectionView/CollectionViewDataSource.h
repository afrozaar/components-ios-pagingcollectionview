//
//  CollectionViewDataSource.h
//  SideScrollCustomLayoutCollectionView
//
//  Created by Grant Spilsbury on 2014/10/20.
//  Copyright (c) 2014 GNS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectionViewCell.h"

@interface CollectionViewDataSource : NSObject <UICollectionViewDataSource>
@property (strong, nonatomic) NSArray *items;
- (id)itemAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView ;
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;

@end
