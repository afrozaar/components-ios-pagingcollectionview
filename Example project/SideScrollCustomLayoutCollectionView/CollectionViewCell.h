//
//  CollectionViewCell.h
//  SideScrollCustomLayoutCollectionView
//
//  Created by Grant Spilsbury on 2014/10/17.
//  Copyright (c) 2014 GNS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
