//
//  MainViewController.m
//  SideScrollCustomLayoutCollectionView
//
//  Created by Grant Spilsbury on 2014/10/20.
//  Copyright (c) 2014 GNS. All rights reserved.
//

#import "MainViewController.h"
#import "CollectionViewCell.h"
#import "CollectionView.h"
#import "CollectionViewDataSource.h"

@interface MainViewController ()
@property (strong, nonatomic) CollectionViewDataSource *dataSource;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *yellowHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pinkConstraint;
@property (weak, nonatomic) IBOutlet CollectionView *collectionView;
@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.dataSource = [[CollectionViewDataSource alloc] init];
    self.collectionView.dataSource = self.dataSource;
    self.collectionView.delegate = self;
    
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([CollectionViewCell class]) bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"collectionViewCell"];
    
    [self.collectionView setPagingEnabled:YES];
}

- (IBAction)changeButton:(id)sender {
    self.yellowHeightConstraint.constant = self.yellowHeightConstraint.constant == 0 ? 100 : 0;
    self.pinkConstraint.constant = self.pinkConstraint.constant == 0 ? 100 : 0;
    [self.view layoutSubviews];
}

@end
