//
//  MainViewController.h
//  SideScrollCustomLayoutCollectionView
//
//  Created by Grant Spilsbury on 2014/10/20.
//  Copyright (c) 2014 GNS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController <UICollectionViewDelegate>


@end
