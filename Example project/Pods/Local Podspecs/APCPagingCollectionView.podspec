Pod::Spec.new do |s|
  s.name         = "APCPagingCollectionView"
  s.version      = "0.0.1"
  s.summary      = "A short description of APCPagingCollectionView."

  s.description  = <<-DESC
                   A longer description of APCPagingCollectionView in Markdown format.

                   * Think: Why did you write this? What is the focus? What does it do?
                   * CocoaPods will be using this to generate tags, and improve search results.
                   * Try to keep it short, snappy and to the point.
                   * Finally, don't worry about the indent, CocoaPods strips it!
                   DESC

  s.homepage     = "https://bitbucket.org/afrozaar/components-ios-pagingcollectionview"
  s.author             = { "Grant Spilsbury" => "grantspilsbury@gmail.com" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "git@bitbucket.org:afrozaar/components-ios-pagingcollectionview.git" }
  s.source_files  = "APCPagingCollectionView", "APCPagingCollectionView/**/*.{h,m}"
  s.requires_arc = true
end