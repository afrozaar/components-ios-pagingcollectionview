//
//  APCCustomHorizontalLayout.m
//  APCPagingCollectionView
//
//  Created by Grant Spilsbury on 2014/10/17.
//  Copyright (c) 2014 GNS. All rights reserved.
//

#import "APCCustomHorizontalLayout.h"

@interface APCCustomHorizontalLayout ()
@property (nonatomic) NSIndexPath *cachedIndexPath;
@property (nonatomic,assign) BOOL animatingBoundsChange;
@property (nonatomic, assign) NSUInteger numberOfItems;
@end

@implementation APCCustomHorizontalLayout

- (void)prepareLayout {
    [super prepareLayout];
    self.collectionView.pagingEnabled = YES;
    self.numberOfItems = [self.collectionView.dataSource collectionView:self.collectionView numberOfItemsInSection:0];
}

- (CGSize)collectionViewContentSize {
    return CGSizeMake(CGRectGetWidth(self.collectionView.bounds) * self.numberOfItems , CGRectGetHeight(self.collectionView.bounds));
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSMutableArray *layoutAttributes = [NSMutableArray array];
    NSArray *visibleIndexPaths = [self indexPathsForElementsInRect:rect];
    for (NSIndexPath *indexPath in [NSSet setWithArray:visibleIndexPaths]) {
        UICollectionViewLayoutAttributes *attributes = [self layoutAttributesForItemAtIndexPath:indexPath];
        [layoutAttributes addObject:attributes];
    }
    return layoutAttributes;
}

- (CGRect)frameForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGRect newFrame = CGRectZero;
    newFrame.size = self.collectionView.bounds.size;
    newFrame.origin = CGPointMake(indexPath.row * CGRectGetWidth(self.collectionView.bounds), CGRectGetMinY(self.collectionView.bounds));
    return newFrame;
}

- (NSArray *)indexPathsForElementsInRect:(CGRect)rect {
    CGSize contentSize = [self collectionViewContentSize];
    if (!CGRectIntersectsRect(rect, CGRectMake(0, 0, contentSize.width, contentSize.height)) || self.numberOfItems < 1) {
        return nil;
    }
    NSInteger startingIndex = MIN([self indexForItemAtPoint:rect.origin], 0);
    NSInteger endIndex = MIN([self indexForItemAtPoint:CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect))], self.numberOfItems - 1);
    
    NSMutableArray *indexPaths = [NSMutableArray array];
    for (NSInteger index = startingIndex; index <= endIndex; index++) {
        [indexPaths addObject:[NSIndexPath indexPathForItem:index inSection:0]];
    }
    return indexPaths;
}

- (NSInteger)indexForItemAtPoint:(CGPoint)point {
    if (point.x < 0 || point.x > [self collectionViewContentSize].width) {
        return NSNotFound;
    }
    return ((NSInteger)floor(point.x/CGRectGetWidth(self.collectionView.bounds)));
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < 0 || indexPath.row > self.numberOfItems - 1) {
        return nil;
    }
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    attributes.frame = [self frameForItemAtIndexPath:indexPath];
    return attributes;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    BOOL shouldInvalidate = !(CGSizeEqualToSize(self.collectionView.bounds.size, newBounds.size));
    if (shouldInvalidate) {
        self.cachedIndexPath = [self.collectionView indexPathForItemAtPoint:self.collectionView.contentOffset];
        [self invalidateLayout];
    }
    return shouldInvalidate;
}

- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset {
    //When rotating the collection view we need it to stay centered on the current visible cell
    //this method is generally called after rotation and so we fetch the visible cell
    //and enforce that it always scrolls to the visible cells origin.
    //this method is also called when the collection view is reloaded and so we dont want to break that behaviour too.
    
    if (self.animatingBoundsChange) {
        CGRect visibleCellFrame = [self frameForItemAtIndexPath:self.cachedIndexPath];
        CGFloat visibleCellMinX = visibleCellFrame.origin.x;
        CGFloat destinationX = proposedContentOffset.x;
        
        if (fabsf(destinationX - visibleCellMinX) > 1.f) {//arbitrary number
            return CGPointMake(visibleCellMinX, proposedContentOffset.y);
        } else {
            return [super targetContentOffsetForProposedContentOffset:proposedContentOffset];
        }
    } else {
        return [super targetContentOffsetForProposedContentOffset:proposedContentOffset];
    }
}

- (void)prepareForAnimatedBoundsChange:(CGRect)oldBounds {
    [super prepareForAnimatedBoundsChange:oldBounds];
    self.animatingBoundsChange = YES;
}

- (void)finalizeAnimatedBoundsChange {
    [super finalizeAnimatedBoundsChange];
    self.animatingBoundsChange = NO;
}

- (UICollectionViewLayoutAttributes *)initialLayoutAttributesForAppearingItemAtIndexPath:(NSIndexPath *)itemIndexPath {
    UICollectionViewLayoutAttributes *attr = [super initialLayoutAttributesForAppearingItemAtIndexPath:itemIndexPath];
    if (self.animatingBoundsChange) {
        return [self layoutAttributesForItemAtIndexPath:itemIndexPath];
    }
    return attr;
}

- (UICollectionViewLayoutAttributes *)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath {
    if (self.animatingBoundsChange) {
        // If the view is rotating, disappearing items should animate to their new attributes.
        return [self layoutAttributesForItemAtIndexPath:itemIndexPath];
    }
    return [super finalLayoutAttributesForDisappearingItemAtIndexPath:itemIndexPath];
}

@end
