//
//  APCCustomHorizontalLayout.h
//  APCPagingCollectionView
//
//  Created by Grant Spilsbury on 2014/10/17.
//  Copyright (c) 2014 GNS. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The `APCCustomHorizontalLayout` is designed to allow for a horizontal paging layout.
 It also centers the collection view on rotation.
 */
@interface APCCustomHorizontalLayout : UICollectionViewLayout
@end
